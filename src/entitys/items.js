
class Items {

    constructor() {
         this.lista = [{
            id:0,
            marca:'desconocida',
            modelo:'desconocido',
            year:'n/a'
         }];
    }
    getItems() {
        return this.lista;
    }

    getItemById(idItem) {
        return this.lista.find((x) => x.id == idItem)
    }

    deleteItem(idItem) {
        this.lista = this.lista.filter( (item) => item.id !== idItem);
    }

    createItem(itemNuevo) {
        this.lista.forEach((item) => {
            if(item.id >= itemNuevo.id) {
                itemNuevo.id = (item.id + 1);
            }
        })

        this.lista.push(itemNuevo);
    }

    editItem(itemEdit) {
        this.lista = this.lista.map((item) => {
            return item.id === itemEdit.id ? itemEdit : item
        })
    }


}
export default Items;
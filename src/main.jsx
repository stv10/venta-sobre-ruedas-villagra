import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeEntity from "./components/datos/HomeEntity";
import 'bootstrap/dist/css/bootstrap.min.css'
import './styles/varios.css'
import Opciones from "./components/Opciones";
import Datos from "./components/Datos";
import About from "./components/About";
import FormEntity from "./components/Form";
import Items from "./entitys/items";
import Home from "./components/Home";
import Lista from "./components/Lista";
const autos = new Items();
const motos = new Items();
const bicis = new Items();

ReactDOM.createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home />}>
        <Route path='opciones' element={<Opciones />}/>
        <Route path="datos" element={<Datos />}>
          <Route path="autos" element={<HomeEntity repoTipo={autos} entidad={'autos'} />}>
            <Route path="lista" element={<Lista  />}/>
            <Route path="form" element={<FormEntity entidad={'autos'} edit={false}  />}>
              <Route path=":id" element={<FormEntity entidad={'autos'} edit={true} />}/>
            </Route>
          </Route>
          <Route path="motos" element={<HomeEntity repoTipo={motos} entidad={'motos'} />}>
            <Route path="lista" element={<Lista />}/>
            <Route path="form" element={<FormEntity entidad={'motos'} edit={false} />}>
              <Route path=":id" element={<FormEntity entidad={'motos'} edit={true} />}/>
            </Route>
          </Route>
          <Route path="bicicletas" element={<HomeEntity repoTipo={bicis} entidad={'bicicletas'} />}>
            <Route path="lista" element={<Lista />}/>
            <Route path="form" element={<FormEntity entidad={'bicicletas'} edit={false} />}>
              <Route path=":id" element={<FormEntity entidad={'bicicletas'} edit={true} />}/>
            </Route>
          </Route>
        </Route>
        <Route path="about" element={<About />}/>
      </Route>
    </Routes>
  </BrowserRouter>
);

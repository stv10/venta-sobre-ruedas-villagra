import { useState } from "react";
import {Link, NavLink, Outlet} from 'react-router-dom'

function Home() {
    const opciones = {
        situacionIva: ['Monotributista','Responsable Inscripto', 'Excento'],
        tipoEmpresa: ['Agente de Retencion', 'Importador'],
        tipoApp: ['No sabe/No contesta', 'Create-React-App', 'Vite'],
        review: ['Nada', 'No demasiado', 'Ni fu ni fa', 'Un poco', 'Mucho']
    }
    const [estado, setEstado] = useState({
        brand: 'Brand',
        situacionIva: 1,
        tipoEmpresa: 1,
        tipoApp: 1,
        review: 1
    });
    return (
        <div>
            <nav className="navbar navbar-dark navbar-expand bg-dark">
                <div className="container-fluid mb-2">
                    <div className="align-items-center">
                        <Link to='/' className="navbar-brand">{estado.brand}</Link>
                    </div>
                    <div className="container ms-0 align-items-center">
                        <ul className="navbar-nav me-auto">
                            <li className="nav-item">
                                <NavLink to='opciones' className='nav-link'>Opciones</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to='datos' className='nav-link'>Datos</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to='about' className='nav-link'>About</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <main>
                <Outlet context={[estado, setEstado, opciones]}/>
            </main>
            <footer className=" container-fluid fixed-bottom bg-dark text-muted">
                <div className="text-center p-3 fw-bold">
                    © 2022 Copyright: {' '}
                    <Link className="link-secondary text-decoration-none" to='/'>{estado.brand}</Link>
                </div>
            </footer>
        </div> 
     );
}

export default Home;
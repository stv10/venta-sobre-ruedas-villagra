import { useState } from 'react';
import {NavLink, Outlet} from 'react-router-dom'
function Datos() {
    
    return ( 
        <>
        <nav className="navbar navbar-expand bg-light navbar-light">
            <div className="container-fluid justify-content-center">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink to='autos' className="nav-link text-active">Autos</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to='motos' className="nav-link text-active">Motos</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to='bicicletas' className="nav-link text-active">Bicicletas</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
        <Outlet />
        </>
     );
}

export default Datos;
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';

const marks = [
  {
    value: 0,
    label: 'Nada',
  },
  {
    value: 1,
    label: 'No demasiado',
  },
  {
    value: 2,
    label: 'Ni fu ni fa',
  },
  {
    value: 3,
    label: 'Un poco',
  },
  {
    value: 4,
    label: 'Mucho!',
  }
];

function valuetext(value) {
  return `${value}°C`;
}

function valueLabelFormat(value) {
  return marks.findIndex((mark) => mark.value === value) + 1;
}


export default function DiscreteSliderValues(props) {
    const {handlerChange , value} = props;

    function handleChange(ev) {
        handlerChange(ev);
    }

  return (
    <Box>
      <Slider
        name='review'
        aria-label="Restricted values"
        value={value}
        valueLabelFormat={valueLabelFormat}
        getAriaValueText={valuetext}
        step={null}
        valueLabelDisplay="auto"
        marks={marks}
        max={4}
        onChange={(ev) => handleChange(ev)}
      />
    </Box>
  );
}

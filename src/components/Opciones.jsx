import { useEffect, useState } from "react";
import { useOutletContext, useNavigate } from "react-router-dom";
import DiscreteSliderValues from "./Slider";
function Opciones() {
  const [estado, setEstado] = useOutletContext();
  const [auxiliar, setAuxiliar] = useState(estado);
  const navigate = useNavigate();

  function handlerSubmit(ev) {
    ev.preventDefault();
    setEstado(auxiliar);
    navigate("/");
  }

  function handlerChange(ev, prop) {
    if(prop === 'tipoEmpresa') {
      setAuxiliar({
        ...auxiliar,
        [prop]: (ev.target.id === 'cBox2' ? 1 : 0)
      })
      return
    }
    if(prop === 'tipoApp') {
      setAuxiliar({
        ...auxiliar,
        [prop]: (ev.target.id == 'radio1' ? 0 : ev.target.id == 'radio2' ? 1 : 2)
      })
      return
    }
    setAuxiliar({
      ...auxiliar,
      [prop]: ev.target.value
    })
  }

  function handlerCancel() {
    navigate("/");
    setAuxiliar(estado);
  }

  return (
    <div className="container mt-3">
      <form
        className="border border-dark p-3"
        onSubmit={(ev) => handlerSubmit(ev)}
      >
        <div className="container-fluid text-center">
          <h2>Opciones</h2>
        </div>
        <div className="row align-items-center mb-3">
          <div className="col-2">
            <label htmlFor="brand" className="col-form-label">
              Nombre Empresa
            </label>
          </div>
          <div className="col-5 col-md-3">
            <input
              type="text"
              id="brand"
              className="form-control"
              value={auxiliar.brand}
              onChange={(ev) => handlerChange(ev,'brand')}
            />
          </div>
        </div>
        <div className="mb-3 row align-items-center">
          <div className="col-2">
            <label htmlFor="iva" className="form-label">
              Condicion frente a IVA
            </label>
          </div>
          <div className="col-5 col-md-3">
            <select
              name="situacionIva"
              id="iva"
              className="form-select"
              value={auxiliar.iva}
              onChange={(ev) => handlerChange(ev,'iva')}
            >
              <option value="select">Seleccione una opcion</option>
              <option value="0">Monotributista</option>
              <option value="1">Responsable Inscripto</option>
              <option value="2">Excento</option>
            </select>
          </div>
        </div>
        <div className="row">
          <div className="col-2"></div>
          <div className="col-3">
            <div className="form-check">
              <input
                type="checkbox"
                name="tipoEmpresa"
                id="cBoxAgente"
                className="form-check-input"
                checked={auxiliar.tipoEmpresa === 0 ? true : false}
                onChange={(ev) => handlerChange(ev,'tipoEmpresa')}
              />
              <label htmlFor="cBoxAgente" className="form-check-label">
                Agente de Retencion
              </label>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-2"></div>
          <div className="col-3">
            <div className="form-check">
              <input
                type="checkbox"
                name="tipoEmpresa"
                id="cBox2"
                className="form-check-input"
                checked={auxiliar.tipoEmpresa === 1 ? true : false}
                onChange={(ev) => handlerChange(ev,'tipoEmpresa')}
              />
              <label htmlFor="cBox2" className="form-check-label">
                Importador
              </label>
            </div>
          </div>
        </div>
        <div className="col-7 col-md-5">
          <label htmlFor="fieldSetApp">Aplicacion creada con</label>
          <div id="fieldSetApp" className="border border-dark p-2 mb-2">
            <div className="form-check">
              <label htmlFor="radio1" className="form-check-label">
                No sabe/No contesta
              </label>
              <input
                type="radio"
                name="tipoApp"
                id="radio1"
                className="form-check-input"
                checked={auxiliar.tipoApp === 0 ? true : false}
                onChange={(ev) => handlerChange(ev,'tipoApp')}
              />
            </div>
            <div className="form-check">
              <label htmlFor="radio2" className="form-check-label">
                Create-React-App
              </label>
              <input
                type="radio"
                name="tipoApp"
                id="radio2"
                className="form-check-input"
                checked={auxiliar.tipoApp === 1 ? true : false}
                onChange={(ev) => handlerChange(ev,'tipoApp')}
              />
            </div>
            <div className="form-check">
              <label htmlFor="radio3" className="form-check-label">
                Vite
              </label>
              <input
                type="radio"
                name="tipoApp"
                id="radio3"
                className="form-check-input"
                checked={auxiliar.tipoApp === 2 ? true : false}
                onChange={(ev) => handlerChange(ev,'tipoApp')}
              />
            </div>
          </div>
        </div>
        <label htmlFor="rangoApp" className="form-label">
          Le gusta esta Aplicación?
        </label>
        <div className="container">
          <DiscreteSliderValues handlerChange={(ev) => handlerChange(ev,'review')} value={auxiliar.review} />
        </div>
        <div className="container-fluid justify-content-end d-flex">
          <button
            className="btn btn-primary border border-radius-4 m-1"
            type="submit"
          >
            Aceptar
          </button>
          <button
            className="btn btn-danger border border-radius-4 m-1"
            type="button"
            onClick={handlerCancel}
          >
            Cancelar
          </button>
        </div>
      </form>
    </div>
  );
}

export default Opciones;

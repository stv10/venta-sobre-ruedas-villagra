import { useEffect, useState } from "react";
import { Outlet, NavLink, useLocation } from "react-router-dom";



function HomeEntity(props) {
    const {repoTipo, entidad} = props;
    const location = useLocation();
    const [items, setItems] = useState(repoTipo.getItems());

    useEffect(() => {
        setItems(repoTipo.getItems());
    },[location])
    
    return ( 
        <>
            <div className="row">
                <div className="col-2">
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <NavLink className="nav-link fs-5" to='lista'>Lista</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link fs-5" to='form'>Nuevo</NavLink>
                        </li>
                    </ul>
                </div>
                <div className="col-10">
                    <Outlet context={[items,entidad,repoTipo]} />
                </div>
            </div>
        </>
     );
}

export default HomeEntity;
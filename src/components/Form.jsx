import { useEffect, useState } from "react";
import { useOutletContext, useParams, useNavigate } from "react-router-dom";
function FormEntity(props) {
    const params = useParams();
    const navigate = useNavigate();
    const itemVacio = {
        id: 0,
        marca: '',
        modelo: '',
        year: ''
    }
    const [items,entidad,repoTipo] = useOutletContext();
    const [item, setItem] = useState(itemVacio);

    useEffect(() => {
        if(params.id) {
            setItem(repoTipo.getItemById(params.id))
        } else {
            setItem(itemVacio);
        }
    },[params])

    const handleSubmit = (ev) => {
        ev.preventDefault();
        if(params.id) {
            repoTipo.editItem(item);
        } else {
            repoTipo.createItem(item);
        }
        navigate(`/datos/${entidad}/lista`);
    }

    const handleEdit = (ev,prop) => {
        setItem({
            ...item,
            [prop]: ev.target.value
        });
    }

    const handleCancel = () => {
        setItem(itemVacio);
        navigate(`/datos/${entidad}/lista`);
    }
    return ( 
        <div className="container">
            <div className="container-fluid text-center mt-3">
                <h4>Datos {entidad}</h4>
            </div>
            <form onSubmit={(ev) => handleSubmit(ev)}>
                <div className="row mx-auto">
                    <div className="col-1 p-0">
                        <label htmlFor="id" className="form-label fs-5">#</label>
                    </div>
                    <div className="col-1 p-0">
                        <span type="text"disabled id="id" className="form-text fs-5">{params.id ? item.id : ''}</span>
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="marca" className="form-label">Marca</label>
                    <input type="text" id="marca" className="form-control" value={item.marca}
                    onChange={(ev) => handleEdit(ev,'marca')} />
                </div>
                <div className="mb-3">
                    <label htmlFor="modelo" className="form-label">Modelo</label>
                    <input type="text" id="modelo" className="form-control" value={item.modelo}
                    onChange={(ev) => handleEdit(ev,'modelo')}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="year" className="form-label">A&ntilde;o</label>
                    <input type="text" id="year" className="form-control" value={item.year}
                    onChange={(ev) => handleEdit(ev,'year')}/>
                </div>
                <div className="container-fluid">
                    <button type="submit" className="btn btn-success m-1">Aceptar</button>
                    <button type="button" className="btn btn-primary m-1" onClick={handleCancel}>Cancelar</button>
                </div>
            </form>
        </div>
     );
}

export default FormEntity;
import { useOutletContext } from "react-router-dom";
function About() {
    const [estado,setEstado,opciones] = useOutletContext();
    return ( 
        <>
        <div className="container text-center justify-content center">
            <h2 className="">About</h2>
            <div className="container">
                <p className="fs-5 text-muted mb-1 d-inline col-6">@{estado.brand} </p>
                <p className="d-inline fs-5 text-active col-6">| Venta sobre ruedas v1.0</p>
            </div>
            <div className="container">
                <p className="fs-5 text-muted mb-1 d-inline col-6">Situacion frente a IVA de la empresa : </p>
                <p className="d-inline fs-5 text-active col-6">{opciones.situacionIva[estado.situacionIva]}</p>
            </div>
            <div className="container">
                <p className="fs-5 text-muted mb-1 d-inline">Tipo de empresa elegida : </p>
                <p className="d-inline fs-5 text-active col-6">{opciones.tipoEmpresa[estado.tipoEmpresa]}</p>
            </div>
            <div className="container">
                <p className="fs-5 text-muted mb-1 d-inline">Review de la satisfaccion con la App : </p>
                <p className="d-inline fs-5 text-active col-6">{opciones.review[estado.review]}</p>
            </div>
            <div className="container">
                <p className="fs-5 text-muted mb-1 d-inline">Tipo de app creada: </p>
                <p className="d-inline fs-5 text-active col-6">{opciones.tipoApp[estado.tipoApp]}</p>
            </div>
        </div>
        </>
     );
}

export default About;
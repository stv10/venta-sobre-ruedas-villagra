import { useOutletContext, useNavigate } from "react-router-dom";

function Lista(props) {
    const navigate = useNavigate();
    const [items,entidad,repoTipo] = useOutletContext();
    const properties = ['id','marca','modelo','year']
    
    function handleEdit(id) {
        navigate(`/datos/${entidad}/form/${id}`);
    }

    function handleDelete(id) {
        repoTipo.deleteItem(id);
        navigate(`/datos/${entidad}/lista`)
    }
    return ( 
        <>
        <div className="container text-center mt-2">
            <h3 className="text-capitalize">{entidad}</h3>
        </div>
        <div className="container">
        <table className="table table-stripped table-bordered">
            <thead>
                <tr>
                    {
                        properties.map((item, index) => {
                            let p = (item === 'year' ? 'año' : item);
                            return (
                                <th key={index} className='text-capitalize' scope="col">{p}</th>
                            );
                        })
                    }
                    <th className="col-2" key={properties.length}>Acciones</th>
                </tr>
            </thead>
            <tbody>
                {
                    items.map((item) => {
                        return (
                            <tr key={item[properties[0]]}>
                                {
                                    properties.map((prop, index) => {
                                        return <td key={index}>{item[prop]}</td>
                                    })
                                }
                                <td className="col-2" key={properties.length}>
                                    <button className="btn btn-danger btn-sm m-1" typeof="button" onClick={() => handleDelete(item.id)}>Eliminar</button> {" "}
                                    <button className="btn btn-primary btn-sm m-1" typeof="button" onClick={() => handleEdit(item.id)}>Editar</button>
                                </td>
                            </tr>
                        );
                    })
                }
            </tbody>
        </table>
        </div>
        </>
     );
}

export default Lista;